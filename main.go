package main

import (
	"fmt"
	"math/rand"
)

func main() {

	stayWin := 0
	switchWin := 0
	repetitions := 10000
	for i := 1; i <= repetitions; i++ {
		secretDoor := rand.Intn(3)
		myDoor := rand.Intn(3)
		switch secretDoor {
		case myDoor:
			stayWin++
		default:
			switchWin++
		}
	}
	// Following lines convert the answer into a percentage (in a very clunky way)
	stayFloat := float32(stayWin)
	switchFloat := float32(switchWin)
	repFloat := float32(repetitions)
	stayPercentage := (stayFloat / repFloat) * 100
	switchPercentage := (switchFloat / repFloat) * 100
	fmt.Printf("Over %d repetitions there was a:\n", repetitions)
	fmt.Println(stayPercentage, "% chance of winning by staying\n", switchPercentage, "% chance of winning by switching")
}
